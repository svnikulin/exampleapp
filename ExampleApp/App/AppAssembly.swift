//
//  AppAssembly.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration

class AppAssembly {
    private let container: Container
    private let assembler: Assembler
    private var modules: [AppModule]
    
    init() {
        container = Container()
        
        container
            .autoregister(QuestionService.self, initializer: QuestionService.init)
            .inObjectScope(.container)
        
        container
            .autoregister(SegmentService.self, initializer: SegmentService.init)
            .inObjectScope(.container)

        assembler = Assembler(container: container)

        modules = [
            MainModule()
        ]

        modules = modules.flatMap(findSubmodules)
        assembler.apply(assemblies: modules.compactMap { $0.createAssembly() })

        container.register(AppAssembly.self) { [unowned self] _ in
            return self
        }
    }
    
    private func findSubmodules(_ module: AppModule) -> [AppModule] {
        var submodules: [AppModule] = [module]

        submodules.append(
            contentsOf: module.submodules.flatMap(findSubmodules)
        )

        return submodules
    }

    func initializeModules() {
        for module in modules {
            module.initializeModule(resolver: container)
        }
    }
}

extension AppAssembly {
    public func resolveMainViewFactory() -> MainViewFactory {
        return container.resolve()
    }
}
