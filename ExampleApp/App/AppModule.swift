//
//  AppModule.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import Swinject

protocol AppModule {
    var submodules: [AppModule] { get }
    
    func createAssembly() -> Assembly?
    
    func initializeModule(resolver: Resolver)
}

extension AppModule {
    var submodules: [AppModule] {
        return []
    }
    
    func createAssembly() -> Assembly? {
        return nil
    }
    
    func initializeModule(resolver: Resolver) {

    }
}

extension Resolver {
    func resolve<Service>() -> Service {
        return resolve(Service.self)!
    }
    
    func resolve<Service, Arg1>(argument: Arg1) -> Service {
        return resolve(Service.self, argument: argument)!
    }
}
