//
//  Appearance.swift
//  ExampleApp
//
//  Created by Семен Никулин on 21.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import UIKit

class Appearance { }

extension Appearance {
    static var appBackgroundColor: UIColor {
        return UIColor(red: 238 / 255, green: 242 / 255, blue: 248 / 255, alpha: 1)
    }
    
    static var appGrayColor: UIColor {
        return UIColor(red: 237 / 255, green: 244 / 255 , blue: 251 / 255, alpha: 1)
    }
    
    static var appTintColor: UIColor {
        return UIColor(red: 113 / 255, green: 151 / 255, blue: 214 / 255, alpha: 1)
    }
}
