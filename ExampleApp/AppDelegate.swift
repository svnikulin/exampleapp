//
//  AppDelegate.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var appAssebbly: AppAssembly!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        
        appAssebbly = AppAssembly()
        appAssebbly.initializeModules()
        
        let mainViewFactory = appAssebbly.resolveMainViewFactory()
        window?.rootViewController = UINavigationController(rootViewController: mainViewFactory.createViewController())
        window?.makeKeyAndVisible()
        return true
    }
}

