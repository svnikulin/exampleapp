//
//  MainModule.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import Swinject

class MainModule: AppModule {
    func createAssembly() -> Assembly? {
        return MainAssembly()
    }
}

class MainAssembly: Assembly {
    private var resolver: Resolver!
    
    func loaded(resolver: Resolver) {
        self.resolver = resolver
    }

    func assemble(container: Container) {
        container
            .register(MainViewController.self) { (resolver) -> MainViewController in
                let viewController = MainViewController()
                resolver.resolve(MainViewReactor.self).map { viewController.bind(to: $0) }
                return viewController
            }
        
        container
            .autoregister(MainViewReactor.self, initializer: MainViewReactor.init)
        
        container
            .autoregister(MainRouter.self, initializer: MainRouter.init)
            .implements(MainPrivateRouter.self)
        
        container
            .autoregister(MainServiceProvider.self, initializer: MainServiceProvider.init)
            .implements(MainServiceProviderProtocol.self)
        
        container
            .register(MainViewFactory.self) { _ in
                self
            }
    }
}

protocol MainViewFactory {
    func createViewController() -> MainViewController
}

extension MainAssembly: MainViewFactory {
    func createViewController() -> MainViewController {
        return resolver.resolve(MainViewController.self)!
    }
}
