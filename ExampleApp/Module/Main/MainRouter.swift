//
//  MainRouter.swift
//  ExampleApp
//
//  Created by Семен Никулин on 21.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

protocol MainPrivateRouter {
    func openWorkOnMistakes()
    func openExam()
    func openLearn()
    func openSearch()
    func openBookmark()
}

class MainRouter {
    
}

extension MainRouter: MainPrivateRouter {
    func openWorkOnMistakes() {
        
    }
    
    func openExam() {
        
    }
    
    func openLearn() {
        
    }
    
    func openSearch() {
        
    }
    
    func openBookmark() {
        
    }
}
