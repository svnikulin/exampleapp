//
//  MainServiceProvider.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

protocol MainServiceProviderProtocol {
    var questionsCount: Int { get }
    var successfulQuestionCount: Int { get }
    var failedQuestionCount: Int { get }
    
    var segmentsCount: Int { get }
    var completedSegmentCount: Int { get }
}

class MainServiceProvider {
    private let questionService: QuestionService
    private let segmentService: SegmentService
    
    init(questionService: QuestionService, segmentService: SegmentService) {
        self.questionService = questionService
        self.segmentService = segmentService
    }
}

extension MainServiceProvider: MainServiceProviderProtocol {
    var questionsCount: Int {
        return questionService.questionsCount
    }
    
    var successfulQuestionCount: Int {
        return questionService.successfulQuestionCount
    }
    
    var failedQuestionCount: Int {
        return questionService.failedQuestionCount
    }
    
    var segmentsCount: Int {
        return segmentService.segmentsCount
    }
    
    var completedSegmentCount: Int {
        return segmentService.completedSegmentCount
    }
}
