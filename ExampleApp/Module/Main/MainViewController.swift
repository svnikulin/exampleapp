//
//  MainViewController.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import StoreKit
import RxSwift
import Utils

class MainViewController: UIViewController {
    private let mainStackView = build(StackScrollView(inset: UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20))) {
        $0.spacing = 15
        $0.axis = .vertical
        $0.showsVerticalScrollIndicator = false
    }
    
    private let statisticView = build(StatisticView()) {
        $0.layer.cornerRadius = 15
    }
    private let learnButton = MainHorizontalButton()
    
    private let buttonsStackView = build(UIStackView()) {
        $0.axis = .horizontal
        $0.spacing = 15
        $0.distribution = .fillEqually
    }
    
    private let searchButton = MainVerticalButton()
    private let bookmarkButton = MainVerticalButton()
    
    var viewModel: ViewModel = .default {
        didSet { render(viewModel: viewModel) }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Тесты"
        view.backgroundColor = Appearance.appBackgroundColor
        setup()
    }
    
    private func setup() {
        view.addSubview(mainStackView)

        mainStackView.addViews {[
            statisticView,
            learnButton,
            buttonsStackView.addViews {[
                searchButton, bookmarkButton
            ]}
        ]}
        
        mainStackView.snp.makeConstraints {
            $0.top.equalTo(safeArea.top)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        buttonsStackView.snp.makeConstraints {
            $0.height.equalTo(searchButton.snp.width)
        }
    }
    
    private func render(viewModel: ViewModel) {
        statisticView.viewModel = viewModel.statisticViewModel
        
        learnButton.viewModel = viewModel.learnViewModel
        searchButton.viewModel = viewModel.searchViewModel
        bookmarkButton.viewModel = viewModel.bookmarkViewModel
    }
}

extension MainViewController {
    struct ViewModel: Mutable {
        var statisticViewModel: StatisticView.ViewModel
        var learnViewModel: MainHorizontalButton.ViewModel
        var searchViewModel: MainVerticalButton.ViewModel
        var bookmarkViewModel: MainVerticalButton.ViewModel
    }
}

extension MainViewController.ViewModel {
    static let `default` = MainViewController.ViewModel(statisticViewModel: .default, learnViewModel: .default, searchViewModel: .default, bookmarkViewModel: .default)
}
