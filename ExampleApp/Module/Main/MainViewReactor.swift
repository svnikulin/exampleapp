//
//  MainViewReactor.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import RxSwift
import RxCocoa
import Utils

class MainViewReactor {
    private let router: MainPrivateRouter
    private let serviceProvider: MainServiceProviderProtocol
    
    private let viewModelSubject = BehaviorSubject<MainViewController.ViewModel>(value: .default)
    
    init(router: MainPrivateRouter, serviceProvider: MainServiceProviderProtocol) {
        self.router = router
        self.serviceProvider = serviceProvider
        
        let successfulQuestionCount = serviceProvider.successfulQuestionCount
        let questionsCount = serviceProvider.questionsCount
        let completedSegmentCount = serviceProvider.completedSegmentCount
        let segmentsCount = serviceProvider.segmentsCount
        
        let questionProgress = roundedPercent(for: successfulQuestionCount,
                                              maxValue: questionsCount)
        let segmentProgress = roundedPercent(for: completedSegmentCount,
                                             maxValue: segmentsCount)
        
        viewModelSubject.mutate {
            $0.statisticViewModel = StatisticView.ViewModel(
                questionViewModel: TitleProgressView.ViewModel(
                    title: "Вопросы",
                    value: "\(successfulQuestionCount)/\(questionsCount)",
                    progress: Float(questionProgress)
                ),
                sectionViewModel: TitleProgressView.ViewModel(
                    title: "Разделы",
                    value: "\(completedSegmentCount)/\(segmentsCount)",
                    progress: Float(segmentProgress)
                ),
                readyViewModel: TitleProgressView.ViewModel(
                    title: "Готовность",
                    value: "\(questionProgress)%",
                    progress: Float(questionProgress)
                ),
                mistakeViewModel: StatisticButton.ViewModel(
                    name: "Мои ошибки",
                    content: .text("\(serviceProvider.failedQuestionCount)"),
                    onTap: Command(closure: weakify(self, type(of: self).tapOnWorkOnMistakes))
                ),
                examViewModel: StatisticButton.ViewModel(
                    name: "Сдать экзамен",
                    content: .image(UIImage(named: "main/lawyer")),
                    onTap: Command(closure: weakify(self, type(of: self).tapOnExam))
                )
            )
            
            $0.learnViewModel = MainHorizontalButton.ViewModel(
                image: UIImage(named: "main/learn"),
                title: "Подготовка",
                subtitle: "Выберите раздел, изучайте вопросы, тренируйтесь",
                onTap: Command(closure: weakify(self, type(of: self).tapOnLearn))
            )
            
            $0.searchViewModel = MainVerticalButton.ViewModel(
                image: UIImage(named: "main/search"),
                title: "Поиск",
                subtitle: "Быстро находите вопрос по слову или номеру",
                onTap: Command(closure: weakify(self, type(of: self).tapOnSearch))
            )
               
            $0.bookmarkViewModel = MainVerticalButton.ViewModel(
                image: UIImage(named: "main/bookmark"),
                title: "Избранное",
                subtitle: "Свайпом влево, добавляйте сложные вопросы",
                onTap: Command(closure: weakify(self, type(of: self).tapOnBookmark))
            )
        }
    }
    
    private func tapOnWorkOnMistakes() {
        router.openWorkOnMistakes()
    }
    
    private func tapOnExam() {
        router.openExam()
    }
    
    private func tapOnLearn() {
        router.openLearn()
    }
    
    private func tapOnSearch() {
        router.openSearch()
    }
    
    private func tapOnBookmark() {
        router.openBookmark()
    }
    
    var rx_viewModel: Observable<MainViewController.ViewModel> {
        return viewModelSubject.asObservable()
    }
}

extension MainViewController {
    public func bind(to viewReactor: MainViewReactor) {
        func holdRef(_ any: Any) {}
        
        viewReactor.rx_viewModel
            .takeUntil(rx.deallocating)
            .bind { [weak self] viewModel in
                holdRef(viewReactor)
                self?.viewModel = viewModel
            }
    }
}
