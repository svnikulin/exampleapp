//
//  MainHorizontalButton.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import Utils

private let topBottomPadding: CGFloat = 12

private var titleFontSize: CGFloat {
    if DeviceType.isIPhone5orSmaller { return 22 }
    return 24
}

private var subtitleFontSize: CGFloat {
    if DeviceType.isIPhone5orSmaller { return 11 }
    return 12
}

class MainHorizontalButton: UIButton {
    private let iconImageView = build(UIImageView()) {
        $0.contentMode = .scaleAspectFit
        $0.isUserInteractionEnabled = false
    }
    
    private let nameLabel = build(UILabel()) {
        $0.font = UIFont.systemFont(ofSize: titleFontSize, weight: .semibold)
        $0.numberOfLines = 2
        $0.textColor = UIColor(red: 62 / 255, green: 66 / 255, blue: 87 / 255, alpha: 1)
    }
    
    private let subtitleLabel = build(UILabel()) {
        $0.font = UIFont.systemFont(ofSize: subtitleFontSize, weight: .medium)
        $0.numberOfLines = 3
        $0.textColor = UIColor(red: 99 / 255, green: 102 / 255, blue: 114 / 255, alpha: 1)
    }
    
    public var viewModel: ViewModel = .default {
        didSet { render(viewModel: viewModel) }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        layer.cornerRadius = 15
        
        addSubview(iconImageView)
        addSubview(nameLabel)
        addSubview(subtitleLabel)
        
        iconImageView.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().inset(16)
            $0.width.height.equalTo(50)
        }
        
        nameLabel.snp.makeConstraints {
            $0.top.equalToSuperview().inset(topBottomPadding)
            $0.leading.equalTo(iconImageView.snp.trailing).offset(16)
            $0.trailing.equalToSuperview()
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(nameLabel.snp.bottom).offset(8)
            $0.leading.equalTo(nameLabel.snp.leading)
            $0.trailing.equalToSuperview().inset(16)
            $0.bottom.equalToSuperview().inset(topBottomPadding)
        }
        
        addTarget(self, action: #selector(onTap), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func render(viewModel: ViewModel) {
        iconImageView.image = viewModel.image
        
        nameLabel.text = viewModel.title
        subtitleLabel.text = viewModel.subtitle
    }
    
    @objc private func onTap() {
        viewModel.onTap.execute()
    }
    
    override var isHighlighted: Bool {
        didSet {
            UIView.animate(withDuration: 0.25, delay: 0, options: [.beginFromCurrentState, .allowUserInteraction], animations: {
                self.alpha = self.isHighlighted ? 0.8 : 1
            }, completion: nil)
        }
    }
}

extension MainHorizontalButton {
    struct ViewModel: Mutable {
        let image: UIImage?
        let title: String
        let subtitle: String
        let onTap: Command
    }
}

extension MainHorizontalButton.ViewModel {
    static let `default` = MainHorizontalButton.ViewModel(image: nil, title: "", subtitle: "", onTap: .noop)
}
