//
//  MainVerticalButton.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import Utils

private var topPadding: CGFloat {
    if DeviceType.isIPhone5orSmaller { return 12 }
    return 16
}

private var topSubtitlePadding: CGFloat {
    if DeviceType.isIPhone5orSmaller { return 8 }
    return 10
}

private var leftRightPadding: CGFloat {
    if DeviceType.isIPhone5orSmaller { return 12 }
    return 16
}

private var titleFontSize: CGFloat {
    if DeviceType.isIPhone5orSmaller { return 16 }
    return 18
}

private var subtitleFontSize: CGFloat {
    if DeviceType.isIPhone5orSmaller { return 11 }
    return 12
}

class MainVerticalButton: UIButton {
    private let iconImageView = build(UIImageView()) {
        $0.contentMode = .scaleAspectFit
    }
    
    private let nameLabel = build(UILabel()) {
        $0.font = UIFont.systemFont(ofSize: titleFontSize, weight: .semibold)
        $0.numberOfLines = 2
        $0.textColor = UIColor(red: 62 / 255, green: 66 / 255, blue: 87 / 255, alpha: 1)
    }
    
    private let subtitleLabel = build(UILabel()) {
        $0.font = UIFont.systemFont(ofSize: subtitleFontSize, weight: .medium)
        $0.numberOfLines = 3
        $0.textColor = UIColor(red: 99 / 255, green: 102 / 255, blue: 114 / 255, alpha: 1)
    }
    
    public var viewModel: ViewModel = .default {
        didSet { render(viewModel: viewModel) }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        layer.cornerRadius = 15
        
        addSubview(iconImageView)
        addSubview(nameLabel)
        addSubview(subtitleLabel)
        
        iconImageView.snp.makeConstraints {
            $0.top.leading.equalToSuperview().inset(topPadding)
            $0.width.height.equalTo(30)
        }
        
        nameLabel.snp.makeConstraints {
            $0.top.equalTo(iconImageView.snp.bottom).offset(topPadding)
            $0.leading.trailing.equalToSuperview().inset(leftRightPadding)
        }
        
        subtitleLabel.snp.makeConstraints {
            $0.top.equalTo(nameLabel.snp.bottom).offset(topSubtitlePadding)
            $0.leading.trailing.equalToSuperview().inset(leftRightPadding)
        }
        
        addTarget(self, action: #selector(onTap), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func render(viewModel: ViewModel) {
        iconImageView.image = viewModel.image
        
        nameLabel.text = viewModel.title
        subtitleLabel.text = viewModel.subtitle
    }
    
    @objc private func onTap() {
        viewModel.onTap.execute()
    }
    
    override var isHighlighted: Bool {
        didSet {
            UIView.animate(withDuration: 0.25, delay: 0, options: [.beginFromCurrentState, .allowUserInteraction], animations: {
                self.alpha = self.isHighlighted ? 0.8 : 1
            }, completion: nil)
        }
    }
}

extension MainVerticalButton {
    struct ViewModel: Mutable {
        let image: UIImage?
        let title: String
        let subtitle: String
        let onTap: Command
    }
}

extension MainVerticalButton.ViewModel {
    static let `default` = MainVerticalButton.ViewModel(image: nil, title: "", subtitle: "", onTap: .noop)
}
