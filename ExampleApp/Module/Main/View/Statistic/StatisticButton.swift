//
//  StatisticButton.swift
//  ExampleApp
//
//  Created by Семен Никулин on 26.09.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import Utils

private var titleSize: CGFloat {
    if DeviceType.isIPhone5orSmaller { return 15 }
    return 17
}

private let contentHeight: CGFloat = 50

class StatisticButton: UIButton {
    private let stackView = build(UIStackView()) {
        $0.axis = .vertical
        $0.spacing = 10
        $0.isUserInteractionEnabled = false
    }
    
    private let iconImageView = build(UIImageView()) {
        $0.contentMode = .scaleAspectFit
    }
    
    private let valueLabel = build(UILabel()) {
        $0.font = UIFont.systemFont(ofSize: 44, weight: .semibold)
        $0.textAlignment = .center
    }
    
    private let nameLabel = build(UILabel()) {
        $0.font = UIFont.systemFont(ofSize: titleSize, weight: .semibold)
        $0.textAlignment = .center
    }
    
    public var contentColor: UIColor = .white
    
    public var viewModel: ViewModel = .default {
        didSet { render(viewModel: viewModel) }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        addSubview(stackView)
        
        stackView.addViews {[
            iconImageView,
            valueLabel,
            nameLabel
        ]}
        
        stackView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview()
            $0.center.equalToSuperview()
        }
        
        iconImageView.snp.makeConstraints {
            $0.height.equalTo(contentHeight)
        }
        
        valueLabel.snp.makeConstraints {
            $0.height.equalTo(contentHeight)
        }
        
        addTarget(self, action: #selector(onTap), for: .touchUpInside)
    }
    
    override var isHighlighted: Bool {
        didSet {
            UIView.animate(withDuration: 0.25, delay: 0, options: [.beginFromCurrentState, .allowUserInteraction], animations: {
                self.alpha = self.isHighlighted ? 0.8 : 1
            }, completion: nil)
        }
    }
    
    private func render(viewModel: ViewModel) {
        showContentView(for: viewModel.content)
        
        nameLabel.text = viewModel.name
        nameLabel.textColor = contentColor
        
        switch viewModel.content {
        case .image(let image):
            iconImageView.image = image
            iconImageView.setImageColor(color: contentColor)
        case .text(let value):
            valueLabel.text = value
            valueLabel.textColor = contentColor
        }
    }
    
    private func showContentView(for content: ViewModel.Content) {
        [iconImageView, valueLabel].forEach { $0.isHidden = true }
        switch content {
        case .image: iconImageView.isHidden = false
        case .text: valueLabel.isHidden = false
        }
    }
    
    @objc private func onTap() {
        viewModel.onTap.execute()
    }
}

extension StatisticButton {
    struct ViewModel: Mutable {
        let name: String
        let content: Content
        let onTap: Command
    }
}

extension StatisticButton.ViewModel {
    enum Content {
        case image(UIImage?)
        case text(String)
    }
    
    static let `default` = StatisticButton.ViewModel(name: "", content: .text(""), onTap: .noop)
}
