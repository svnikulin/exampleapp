//
//  StatisticView.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import Utils

private var leftRightPadding: CGFloat {
    if DeviceType.isIPhone5orSmaller { return 12 }
    return 16
}

private var topBottomPadding: CGFloat {
    if DeviceType.isIPhone5orSmaller { return 15 }
    return 20
}

class StatisticView: UIView {
    private let titleLabel = build(UILabel()) {
        $0.text = "Статистика"
        $0.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
    }
    
    private let stackView = build(UIStackView()) {
        $0.axis = .vertical
        $0.spacing = 15
        $0.distribution = .fill
    }
    
    private let questionsStatisticView = TitleProgressView()
    
    private let statisticVerStackView = build(UIStackView()) {
        $0.axis = .vertical
        $0.spacing = 8
        $0.distribution = .fill
    }
    
    private let statisticHorStackView = build(UIStackView()) {
        $0.axis = .horizontal
        $0.spacing = 15
        $0.distribution = .fillEqually
    }
    
    private let segmentStatisticView = TitleProgressView()
    private let progressStatisticView = TitleProgressView()
    
    private let buttonStackView = build(UIStackView()) {
        $0.axis = .horizontal
        $0.spacing = 15
        $0.distribution = .fillEqually
    }
    
    private let mistakeButton = build(StatisticButton()) {
        $0.backgroundColor = Appearance.appTintColor
        $0.contentColor = .white
        $0.layer.cornerRadius = 15
    }
    
    private let examButton = build(StatisticButton()) {
        $0.backgroundColor = Appearance.appGrayColor
        $0.contentColor = Appearance.appTintColor
        $0.layer.cornerRadius = 15
    }
    
    public var viewModel: ViewModel = .default {
        didSet { render(viewModel: viewModel) }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        addSubview(stackView)
        
        stackView.addViews {[
            titleLabel,
            statisticVerStackView.addViews {[
                questionsStatisticView,
                statisticHorStackView.addViews {[
                    segmentStatisticView, progressStatisticView
                ]}
            ]},
            buttonStackView.addViews {[
                mistakeButton, examButton
            ]}
        ]}
        
        stackView.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(topBottomPadding)
            $0.leading.trailing.equalToSuperview().inset(leftRightPadding)
        }
        
        buttonStackView.snp.makeConstraints {
            $0.height.equalTo(120)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func render(viewModel: ViewModel) {
        questionsStatisticView.viewModel = viewModel.questionViewModel
        segmentStatisticView.viewModel = viewModel.sectionViewModel
        progressStatisticView.viewModel = viewModel.readyViewModel
        
        mistakeButton.viewModel = viewModel.mistakeViewModel
        examButton.viewModel = viewModel.examViewModel
    }
}

extension StatisticView {
    struct ViewModel: Mutable {
        var questionViewModel: TitleProgressView.ViewModel
        var sectionViewModel: TitleProgressView.ViewModel
        var readyViewModel: TitleProgressView.ViewModel
        var mistakeViewModel: StatisticButton.ViewModel
        var examViewModel: StatisticButton.ViewModel
    }
}

extension StatisticView.ViewModel {
    static let `default` = StatisticView.ViewModel(questionViewModel: .default, sectionViewModel: .default, readyViewModel: .default, mistakeViewModel: .default, examViewModel: .default)
}
