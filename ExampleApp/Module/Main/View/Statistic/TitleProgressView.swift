//
//  TitleProgressView.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import Utils

class TitleProgressView: UIView {
    private let labelStackView = build(UIStackView()) {
        $0.axis = .horizontal
        $0.spacing = 0
        $0.distribution = .fill
    }
    
    private let titleLabel = build(UILabel()) {
        $0.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        $0.textColor = UIColor(red: 158 / 255, green: 158 / 255 , blue: 158 / 255, alpha: 1)
        $0.textAlignment = .left
    }
    
    private let valueLabel = build(UILabel()) {
        $0.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        $0.textColor = Appearance.appTintColor
        $0.textAlignment = .right
    }
    
    private let progressView = build(UIProgressView()) {
        $0.trackTintColor = Appearance.appGrayColor
        $0.progressTintColor = Appearance.appTintColor
        $0.layer.cornerRadius = 3
        $0.clipsToBounds = true
    }
    
    public var viewModel: ViewModel = .default {
        didSet { render(viewModel: viewModel) }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(labelStackView)
        addSubview(progressView)
        
        labelStackView.addViews {[
            titleLabel, valueLabel
        ]}
        
        labelStackView.snp.makeConstraints {
            $0.top.leading.trailing.equalToSuperview()
        }
        
        progressView.snp.makeConstraints {
            $0.top.equalTo(labelStackView.snp.bottom).offset(7)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(5.5)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func render(viewModel: ViewModel) {
        titleLabel.text = viewModel.title
        valueLabel.text = viewModel.value
        progressView.progress = viewModel.progress > 1 ? viewModel.progress / 100 : viewModel.progress
    }
}

extension TitleProgressView: Mutable {
    struct ViewModel {
        let title: String
        let value: String
        let progress: Float
    }
}

extension TitleProgressView.ViewModel {
    static let `default` = TitleProgressView.ViewModel(title: "", value: "", progress: 0)
}
