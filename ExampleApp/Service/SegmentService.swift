//
//  SegmentService.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

class SegmentService {
    private let questionService: QuestionService
    
    private let segment = 20
    
    init(questionService: QuestionService) {
        self.questionService = questionService
    }
    
    var segmentsCount: Int {
        let questionsCount = questionService.questionsCount
        var segments = questionsCount / segment
        if questionsCount % segment > segment / 2 { segments += 1 }
        return segments
    }
    
    var completedSegmentCount: Int {
        return segmentsCount / 2
    }
}
