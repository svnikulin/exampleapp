//
//  Command.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

public class CommandWith<T> {
    private let commandClosure: (T) -> ()

    public init(closure: @escaping (T) -> ()) {
        self.commandClosure = closure
    }

    public init() {
        self.commandClosure = { value in }
    }

    public func execute(with param: T) {
        commandClosure(param)
    }

    public static var noop: CommandWith<T> {
        return CommandWith<T>{_ in}
    }
}

public typealias Command = CommandWith<Void>

public extension CommandWith where T == Void {
    func execute() {
        self.execute(with: ())
    }
}
