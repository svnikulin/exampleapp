//
//  DeviceType.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import UIKit

private enum ScreenSize {
    static let screenWidth      = UIScreen.main.bounds.size.width
    static let screenHeight     = UIScreen.main.bounds.size.height
    static let screenMaxLength  = max(ScreenSize.screenWidth, ScreenSize.screenHeight)
    static let screenMinLength  = min(ScreenSize.screenWidth, ScreenSize.screenHeight)
}

public enum DeviceType {
    public static let isIPhone5orSmaller = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.screenMaxLength < 667.0
}
