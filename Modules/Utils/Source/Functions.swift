//
//  Functions.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import Foundation

@discardableResult
public func build<T>(_ object: T, _ initializer: (T) -> Void) -> T {
    initializer(object)
    return object
}

public func roundedPercent(for currentValue: Int, maxValue: Int) -> Int {
    let percent = CGFloat(currentValue) / CGFloat(maxValue) * 100
    return Int(percent.rounded())
}
