//
//  Mutable.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import RxSwift

public protocol Mutable { }

public extension Mutable {
    func mutate(mutation: (inout Self) -> ()) -> Self {
        var val = self
        mutation(&val)
        return val
    }
}

public extension BehaviorSubject where Element: Mutable {
    func mutate(mutation: (inout Element) -> ()) {
        guard let value = try? value() else {
            assertionFailure("Can't mutate element in behavior subject")
            return
        }

        onNext(value.mutate(mutation: mutation))
    }
}
