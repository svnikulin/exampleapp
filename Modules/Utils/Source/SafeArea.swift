//
//  SafeArea.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import SnapKit

public struct SafeArea {
    public let top: ConstraintItem
    public let bottom: ConstraintItem
}

public extension UIViewController {
    var safeArea: SafeArea {
        if #available(iOS 11.0, *) {
            return SafeArea(top: view.safeAreaLayoutGuide.snp.top, bottom: view.safeAreaLayoutGuide.snp.bottom)
        } else {
            return SafeArea(top: topLayoutGuide.snp.bottom, bottom: bottomLayoutGuide.snp.top)
        }
    }
}
