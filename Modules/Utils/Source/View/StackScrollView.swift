//
//  StackScrollView.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import SnapKit
import UIKit

public class StackScrollView: UIScrollView {
    var stackView = UIStackView()
    
    public var axis: NSLayoutConstraint.Axis {
        set { stackView.axis = newValue }
        get { return stackView.axis }
    }
    
    public var spacing: CGFloat {
        set { stackView.spacing = newValue }
        get { return stackView.spacing }
    }

    public var distribution: UIStackView.Distribution {
        set { stackView.distribution = newValue }
        get { return stackView.distribution }
    }

    public var alignment: UIStackView.Alignment {
        set { stackView.alignment = newValue }
        get { return stackView.alignment }
    }
    
    public var arrangedSubviews: [UIView] {
        return stackView.arrangedSubviews
    }
    
    public init(inset: UIEdgeInsets = .zero) {
        super.init(frame: .zero)
        addSubview(stackView)
        
        stackView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(inset.top)
            make.leading.equalToSuperview().inset(inset.left)
            make.trailing.equalToSuperview().inset(inset.right)
            make.bottom.equalToSuperview().inset(inset.bottom)
            make.width.equalToSuperview().offset(-(inset.left+inset.right))
        }
    }
    
    required  init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func addArrangedSubviews(_ views: UIView...) {
        views.forEach {
            stackView.addArrangedSubview($0)
        }
    }
}

extension StackScrollView: ArrangedSubviewProtocol {
    @discardableResult
    public func addViews(closure: () -> [UIView]) -> Self {
        stackView.addViews(closure: closure)
        return self
    }
}


