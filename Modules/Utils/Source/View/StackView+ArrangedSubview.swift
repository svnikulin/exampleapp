//
//  StackView+ArrangedSubview.swift
//  ExampleApp
//
//  Created by Семен Никулин on 26.09.2020.
//  Copyright © 2020 Example. All rights reserved.
//

import UIKit

public protocol ArrangedSubviewProtocol {
    func addViews(closure: () -> [UIView]) -> Self
}

extension UIStackView: ArrangedSubviewProtocol {
    @discardableResult
    public func addViews(closure: () -> [UIView]) -> Self {
        closure().forEach(addArrangedSubview)
        return self
    }
}
