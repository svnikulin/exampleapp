//
//  Weak.swift
//  ExampleApp
//
//  Created by Семен Никулин on 20.08.2020.
//  Copyright © 2020 Example. All rights reserved.
//

public func weakify <T: AnyObject, U>(_ owner: T, _ f: @escaping (T)->() throws ->()) -> (U) throws -> () {
    return { [weak owner] _ in
        if let this = owner {
            try f(this)()
        }
    }
}

public func weakify <T: AnyObject, U>(_ owner: T, _ f: @escaping (T) -> (U) -> ()) -> (U) -> () {
    return { [weak owner] obj in
        if let this = owner {
            f(this)(obj)
        }
    }
}

public func weakify <T: AnyObject, U>(_ owner: T, _ f: @escaping (T) -> (U) throws -> ()) -> (U) throws -> () {
    return { [weak owner] obj in
        if let this = owner {
            try f(this)(obj)
        }
    }
}

public func weakify <T: AnyObject, U>(_ owner: T, _ f: @escaping (T)->()->U) -> () -> U? {
    return { [weak owner] in
        if let this = owner {
            return f(this)()
        } else {
            return nil
        }
    }
}

public func weakify <T: AnyObject, U>(_ owner: T, _ f: @escaping (T) -> () throws -> U) -> () throws -> U? {
    return { [weak owner] in
        if let this = owner {
            return try f(this)()
        } else {
            return nil
        }
    }
}

public func weakify <T: AnyObject, U, V>(_ owner: T, _ f: @escaping (T)->(U)->V) -> (U) -> V? {
    return { [weak owner] obj in
        if let this = owner {
            return f(this)(obj)
        } else {
            return nil
        }
    }
}

public func weakify <T: AnyObject, U, V>(_ owner: T, _ f: @escaping (T) -> (U) throws -> V) -> (U) throws -> V? {
    return { [weak owner] obj in
        if let this = owner {
            return try f(this)(obj)
        } else {
            return nil
        }
    }
}

public func weakify <T: AnyObject>(_ owner: T, _ f: @escaping  (T) -> ()->()) -> ()->() {
    return { [weak owner] in
        if let this = owner {
            f(this)()
        }
    }
}
